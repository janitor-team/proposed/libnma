# translation of NetworkManager to Kinyarwanda.
# Copyright (C) 2005 Free Software Foundation, Inc.
# This file is distributed under the same license as the NetworkManager package.
# Steve Murphy <murf@e-tools.com>, 2005
# Steve performed initial rough translation from compendium built from translations provided by the following translators:
# Philibert Ndandali  <ndandali@yahoo.fr>, 2005.
# Viateur MUGENZI <muvia1@yahoo.fr>, 2005.
# Noëlla Mupole <s24211045@tuks.co.za>, 2005.
# Carole Karema <karemacarole@hotmail.com>, 2005.
# JEAN BAPTISTE NGENDAHAYO <ngenda_denis@yahoo.co.uk>, 2005.
# Augustin KIBERWA  <akiberwa@yahoo.co.uk>, 2005.
# Donatien NSENGIYUMVA <ndonatienuk@yahoo.co.uk>, 2005..
#
msgid ""
msgstr ""
"Project-Id-Version: NetworkManager HEAD\n"
"Report-Msgid-Bugs-To: https://gitlab.gnome.org/GNOME/libnma/\n"
"POT-Creation-Date: 2021-08-20 12:16+0200\n"
"PO-Revision-Date: 2005-03-31 20:55-0700\n"
"Last-Translator: Steve Murphy <murf@e-tools.com>\n"
"Language-Team: Kinyarwanda <translation-team-rw@lists.sourceforge.net>\n"
"Language: rw\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#: org.gnome.nm-applet.gschema.xml.in:6
msgid "Disable connected notifications"
msgstr ""

#: org.gnome.nm-applet.gschema.xml.in:7
msgid "Set this to true to disable notifications when connecting to a network."
msgstr ""

#: org.gnome.nm-applet.gschema.xml.in:11
msgid "Disable disconnected notifications"
msgstr ""

#: org.gnome.nm-applet.gschema.xml.in:12
msgid ""
"Set this to true to disable notifications when disconnecting from a network."
msgstr ""

#: org.gnome.nm-applet.gschema.xml.in:16
msgid "Disable VPN notifications"
msgstr ""

#: org.gnome.nm-applet.gschema.xml.in:17
msgid ""
"Set this to true to disable notifications when connecting to or "
"disconnecting from a VPN."
msgstr ""

#: org.gnome.nm-applet.gschema.xml.in:21
msgid "Suppress networks available notifications"
msgstr ""

#: org.gnome.nm-applet.gschema.xml.in:22
msgid ""
"Set this to true to disable notifications when Wi-Fi networks are available."
msgstr ""

#: org.gnome.nm-applet.gschema.xml.in:26
msgid "Stamp"
msgstr ""

#: org.gnome.nm-applet.gschema.xml.in:27
msgid "Used to determine whether settings should be migrated to a new version."
msgstr ""

#: org.gnome.nm-applet.gschema.xml.in:31
msgid "Disable WiFi Create"
msgstr ""

#: org.gnome.nm-applet.gschema.xml.in:32
msgid ""
"Set to true to disable creation of adhoc networks when using the applet."
msgstr ""

#: org.gnome.nm-applet.gschema.xml.in:36
msgid "Show the applet in notification area"
msgstr ""

#: org.gnome.nm-applet.gschema.xml.in:37
msgid "Set to FALSE to disable displaying the applet in the notification area."
msgstr ""

#: org.gnome.nm-applet.gschema.xml.in:43 org.gnome.nm-applet.gschema.xml.in:48
msgid "Ignore CA certificate"
msgstr ""

#: org.gnome.nm-applet.gschema.xml.in:44
msgid ""
"Set this to true to disable warnings about CA certificates in EAP "
"authentication."
msgstr ""

#: org.gnome.nm-applet.gschema.xml.in:49
msgid ""
"Set this to true to disable warnings about CA certificates in phase 2 of EAP "
"authentication."
msgstr ""

#: shared/nm-utils/nm-shared-utils.c:793
#, c-format
msgid "object class '%s' has no property named '%s'"
msgstr ""

#: shared/nm-utils/nm-shared-utils.c:800
#, c-format
msgid "property '%s' of object class '%s' is not writable"
msgstr ""

#: shared/nm-utils/nm-shared-utils.c:807
#, c-format
msgid ""
"construct property \"%s\" for object '%s' can't be set after construction"
msgstr ""

#: shared/nm-utils/nm-shared-utils.c:815
#, c-format
msgid "'%s::%s' is not a valid property name; '%s' is not a GObject subtype"
msgstr ""

#: shared/nm-utils/nm-shared-utils.c:824
#, c-format
msgid "unable to set property '%s' of type '%s' from value of type '%s'"
msgstr ""

#: shared/nm-utils/nm-shared-utils.c:835
#, c-format
msgid ""
"value \"%s\" of type '%s' is invalid or out of range for property '%s' of "
"type '%s'"
msgstr ""

#: src/nma-bar-code-widget.c:140
msgid "Network"
msgstr ""

#: src/nma-bar-code-widget.c:157
msgid "Password"
msgstr ""

#: src/nma-bar-code-widget.ui:36
msgid "Scan with your phone or <a href=\"nma:print\">Print</a>"
msgstr ""

#: src/nma-cert-chooser-button.c:153
msgid "(None)"
msgstr ""

#: src/nma-cert-chooser-button.c:161
#, c-format
msgid "Key in %s"
msgstr ""

#: src/nma-cert-chooser-button.c:162
#, c-format
msgid "Certificate in %s"
msgstr ""

#: src/nma-cert-chooser-button.c:181 src/nma-pkcs11-cert-chooser-dialog.c:169
msgid "(Unknown)"
msgstr ""

#: src/nma-cert-chooser-button.c:203 src/nma-cert-chooser-button.c:233
msgid "Select"
msgstr ""

#: src/nma-cert-chooser-button.c:204 src/nma-cert-chooser-button.c:234
msgid "Cancel"
msgstr ""

#: src/nma-cert-chooser-button.c:460
msgid "Select from file…"
msgstr ""

#: src/nma-file-cert-chooser.c:113 src/nma-pkcs11-cert-chooser.c:139
msgid "No certificate set"
msgstr ""

#: src/nma-file-cert-chooser.c:131 src/nma-pkcs11-cert-chooser.c:163
msgid "No key set"
msgstr ""

#: src/nma-file-cert-chooser.c:301 src/nma-pkcs11-cert-chooser.c:369
#, c-format
msgid "Choose a key for %s Certificate"
msgstr ""

#: src/nma-file-cert-chooser.c:305 src/nma-pkcs11-cert-chooser.c:373
#, c-format
msgid "%s private _key"
msgstr ""

#: src/nma-file-cert-chooser.c:309 src/nma-pkcs11-cert-chooser.c:377
#, c-format
msgid "%s key _password"
msgstr ""

#: src/nma-file-cert-chooser.c:313
#, c-format
msgid "Choose %s Certificate"
msgstr ""

#: src/nma-file-cert-chooser.c:317 src/nma-pkcs11-cert-chooser.c:385
#, c-format
msgid "%s _certificate"
msgstr ""

#: src/nma-file-cert-chooser.c:402 src/nma-ws/nma-eap-leap.ui:54
#: src/nma-ws/nma-eap-simple.ui:71 src/nma-ws/nma-ws-leap.ui:55
#: src/nma-ws/nma-ws-sae.ui:56 src/nma-ws/nma-ws-wpa-psk.ui:55
msgid "Sho_w password"
msgstr ""

#: src/nma-mobile-providers.c:787
msgid "Default"
msgstr ""

#: src/nma-mobile-providers.c:976
msgid "My country is not listed"
msgstr ""

#: src/nma-mobile-wizard.c:141
msgid "GSM"
msgstr ""

#: src/nma-mobile-wizard.c:144
msgid "CDMA"
msgstr ""

#: src/nma-mobile-wizard.c:249 src/nma-mobile-wizard.c:281
msgid "Unlisted"
msgstr ""

#: src/nma-mobile-wizard.c:481
msgid "My plan is not listed…"
msgstr ""

#: src/nma-mobile-wizard.c:653
msgid "Provider"
msgstr ""

#: src/nma-mobile-wizard.c:1025
msgid "Installed GSM device"
msgstr ""

#: src/nma-mobile-wizard.c:1028
msgid "Installed CDMA device"
msgstr ""

#: src/nma-mobile-wizard.c:1233
msgid "Any device"
msgstr ""

#: src/nma-mobile-wizard.ui:49
msgid "New Mobile Broadband Connection"
msgstr ""

#: src/nma-mobile-wizard.ui:71
msgid ""
"This assistant helps you easily set up a mobile broadband connection to a "
"cellular (3G) network."
msgstr ""

#: src/nma-mobile-wizard.ui:86
msgid "You will need the following information:"
msgstr ""

#: src/nma-mobile-wizard.ui:101
msgid "Your broadband provider’s name"
msgstr ""

#: src/nma-mobile-wizard.ui:115
msgid "Your broadband billing plan name"
msgstr ""

#: src/nma-mobile-wizard.ui:129
msgid "(in some cases) Your broadband billing plan APN (Access Point Name)"
msgstr ""

#: src/nma-mobile-wizard.ui:143
msgid "Create a connection for _this mobile broadband device:"
msgstr ""

#: src/nma-mobile-wizard.ui:170
msgid "Set up a Mobile Broadband Connection"
msgstr ""

#: src/nma-mobile-wizard.ui:188
msgid "Country or region:"
msgstr ""

#: src/nma-mobile-wizard.ui:228
msgid "Choose your Provider’s Country or Region"
msgstr ""

#: src/nma-mobile-wizard.ui:243
msgid "Select your provider from a _list:"
msgstr ""

#: src/nma-mobile-wizard.ui:288
msgid "I can’t find my provider and I wish to set up the connection _manually:"
msgstr ""

#: src/nma-mobile-wizard.ui:309
msgid "My provider uses GSM technology (GPRS, EDGE, UMTS, HSPA)"
msgstr ""

#: src/nma-mobile-wizard.ui:310
msgid "My provider uses CDMA technology (1xRTT, EVDO)"
msgstr ""

#: src/nma-mobile-wizard.ui:321
msgid "Choose your Provider"
msgstr ""

#: src/nma-mobile-wizard.ui:338
msgid "_Select your plan:"
msgstr ""

#: src/nma-mobile-wizard.ui:365
msgid "Selected plan _APN (Access Point Name):"
msgstr ""

#: src/nma-mobile-wizard.ui:415
msgid ""
"Warning: Selecting an incorrect plan may result in billing issues for your "
"broadband account or may prevent connectivity.\n"
"\n"
"If you are unsure of your plan please ask your provider for your plan’s APN."
msgstr ""

#: src/nma-mobile-wizard.ui:436
msgid "Choose your Billing Plan"
msgstr ""

#: src/nma-mobile-wizard.ui:454
msgid ""
"Your mobile broadband connection is configured with the following settings:"
msgstr ""

#: src/nma-mobile-wizard.ui:468
msgid "Your Device:"
msgstr ""

#: src/nma-mobile-wizard.ui:494
msgid "Your Provider:"
msgstr ""

#: src/nma-mobile-wizard.ui:520
msgid "Your Plan:"
msgstr ""

#: src/nma-mobile-wizard.ui:575
msgid ""
"A connection will now be made to your mobile broadband provider using the "
"settings you selected. If the connection fails or you cannot access network "
"resources, double-check your settings. To modify your mobile broadband "
"connection settings, choose “Network Connections” from the System → "
"Preferences menu."
msgstr ""

#: src/nma-mobile-wizard.ui:589
msgid "Confirm Mobile Broadband Settings"
msgstr ""

#: src/nma-pkcs11-cert-chooser-dialog.c:241
msgid "Error logging in: "
msgstr ""

#: src/nma-pkcs11-cert-chooser-dialog.c:263
#, fuzzy
msgid "Error opening a session: "
msgstr "urusobe Ukwihuza"

#: src/nma-pkcs11-cert-chooser-dialog.ui:20
msgid "_Unlock token"
msgstr ""

#: src/nma-pkcs11-cert-chooser-dialog.ui:116
msgid "Name"
msgstr ""

#: src/nma-pkcs11-cert-chooser-dialog.ui:126
msgid "Issued By"
msgstr ""

#: src/nma-pkcs11-cert-chooser.c:381
#, c-format
msgid "Choose a %s Certificate"
msgstr ""

#: src/nma-pkcs11-cert-chooser.c:389
#, c-format
msgid "%s certificate _password"
msgstr ""

#: src/nma-pkcs11-cert-chooser.c:441
msgid "Sho_w passwords"
msgstr ""

#: src/nma-pkcs11-token-login-dialog.c:132
#, c-format
msgid "Enter %s PIN"
msgstr ""

#: src/nma-pkcs11-token-login-dialog.ui:19 src/nma-vpn-password-dialog.ui:23
#: src/nma-wifi-dialog.c:1107
msgid "_Cancel"
msgstr ""

#: src/nma-pkcs11-token-login-dialog.ui:35
msgid "_Login"
msgstr ""

#: src/nma-pkcs11-token-login-dialog.ui:82
msgid "_Remember PIN"
msgstr ""

#: src/nma-ui-utils.c:36
msgid "Store the password only for this user"
msgstr ""

#: src/nma-ui-utils.c:37
msgid "Store the password for all users"
msgstr ""

#: src/nma-ui-utils.c:38
msgid "Ask for this password every time"
msgstr ""

#: src/nma-ui-utils.c:39
msgid "The password is not required"
msgstr ""

# crashrep/source\all\crashrep.lng:%OK_BUTTON%.text
#: src/nma-vpn-password-dialog.ui:39
#, fuzzy
msgid "_OK"
msgstr "YEGO"

#: src/nma-vpn-password-dialog.ui:73
msgid "Sh_ow passwords"
msgstr ""

#: src/nma-vpn-password-dialog.ui:130
msgid "_Tertiary Password:"
msgstr ""

#: src/nma-vpn-password-dialog.ui:144
msgid "_Secondary Password:"
msgstr ""

#: src/nma-vpn-password-dialog.ui:158
msgid "_Password:"
msgstr ""

#: src/nma-wifi-dialog.c:117
#, fuzzy
msgid "Click to connect"
msgstr "urusobe Ukwihuza"

#: src/nma-wifi-dialog.c:448
msgid "New…"
msgstr ""

#: src/nma-wifi-dialog.c:929
msgctxt "Wifi/wired security"
msgid "None"
msgstr ""

#: src/nma-wifi-dialog.c:945
msgid "WEP 40/128-bit Key (Hex or ASCII)"
msgstr ""

#: src/nma-wifi-dialog.c:952
msgid "WEP 128-bit Passphrase"
msgstr ""

#: src/nma-wifi-dialog.c:967 src/nma-ws/nma-ws-802-1x.c:370
msgid "LEAP"
msgstr ""

#: src/nma-wifi-dialog.c:978
msgid "Dynamic WEP (802.1x)"
msgstr ""

#: src/nma-wifi-dialog.c:990
msgid "WPA & WPA2 Personal"
msgstr ""

#: src/nma-wifi-dialog.c:1006
msgid "WPA & WPA2 Enterprise"
msgstr ""

#: src/nma-wifi-dialog.c:1017
msgid "WPA3 Personal"
msgstr ""

#: src/nma-wifi-dialog.c:1111
msgid "C_reate"
msgstr ""

#: src/nma-wifi-dialog.c:1113
#, fuzzy
msgid "C_onnect"
msgstr "Kwihuza"

#: src/nma-wifi-dialog.c:1191
#, c-format
msgid ""
"Passwords or encryption keys are required to access the Wi-Fi network “%s”."
msgstr ""

#: src/nma-wifi-dialog.c:1193
msgid "Wi-Fi Network Authentication Required"
msgstr ""

#: src/nma-wifi-dialog.c:1195
msgid "Authentication required by Wi-Fi network"
msgstr ""

#: src/nma-wifi-dialog.c:1200
#, fuzzy
msgid "Create New Wi-Fi Network"
msgstr "Gishya"

#: src/nma-wifi-dialog.c:1202
msgid "New Wi-Fi network"
msgstr ""

#: src/nma-wifi-dialog.c:1203
#, fuzzy
msgid "Enter a name for the Wi-Fi network you wish to create."
msgstr "i Na Umutekano Amagenamiterere Bya i urusobe Kuri Kurema"

#: src/nma-wifi-dialog.c:1205
#, fuzzy
msgid "Connect to Hidden Wi-Fi Network"
msgstr "Kuri a urusobe"

#: src/nma-wifi-dialog.c:1207
msgid "Hidden Wi-Fi network"
msgstr ""

#: src/nma-wifi-dialog.c:1208
#, fuzzy
msgid ""
"Enter the name and security details of the hidden Wi-Fi network you wish to "
"connect to."
msgstr "i Na Umutekano Amagenamiterere Bya i urusobe Kuri Kurema"

#: src/nma-ws/nma-eap-fast.c:59
msgid "missing EAP-FAST PAC file"
msgstr ""

#: src/nma-ws/nma-eap-fast.c:256 src/nma-ws/nma-eap-peap.c:312
#: src/nma-ws/nma-eap-ttls.c:366
msgid "GTC"
msgstr ""

#: src/nma-ws/nma-eap-fast.c:272 src/nma-ws/nma-eap-peap.c:280
#: src/nma-ws/nma-eap-ttls.c:300
msgid "MSCHAPv2"
msgstr ""

#: src/nma-ws/nma-eap-fast.c:395
msgid "Choose a PAC file"
msgstr ""

#: src/nma-ws/nma-eap-fast.c:402
msgid "PAC files (*.pac)"
msgstr ""

#: src/nma-ws/nma-eap-fast.c:406
msgid "All files"
msgstr ""

#: src/nma-ws/nma-eap-fast.ui:23
msgid "Anonymous"
msgstr ""

#: src/nma-ws/nma-eap-fast.ui:26
msgid "Authenticated"
msgstr ""

#: src/nma-ws/nma-eap-fast.ui:29
msgid "Both"
msgstr ""

#: src/nma-ws/nma-eap-fast.ui:42 src/nma-ws/nma-eap-peap.ui:42
#: src/nma-ws/nma-eap-ttls.ui:113
msgid "Anony_mous identity"
msgstr ""

#: src/nma-ws/nma-eap-fast.ui:68
msgid "PAC _file"
msgstr ""

#: src/nma-ws/nma-eap-fast.ui:107 src/nma-ws/nma-eap-peap.ui:115
#: src/nma-ws/nma-eap-ttls.ui:71
#, fuzzy
msgid "_Inner authentication"
msgstr "Icyerekezo"

#: src/nma-ws/nma-eap-fast.ui:136
msgid "Allow automatic PAC pro_visioning"
msgstr ""

#: src/nma-ws/nma-eap-leap.c:55
msgid "missing EAP-LEAP username"
msgstr ""

#: src/nma-ws/nma-eap-leap.c:64
msgid "missing EAP-LEAP password"
msgstr ""

#: src/nma-ws/nma-eap-leap.ui:15 src/nma-ws/nma-eap-simple.ui:15
#: src/nma-ws/nma-ws-leap.ui:15
msgid "_Username"
msgstr ""

#: src/nma-ws/nma-eap-leap.ui:29 src/nma-ws/nma-eap-simple.ui:29
#: src/nma-ws/nma-ws-leap.ui:29 src/nma-ws/nma-ws-sae.ui:14
#: src/nma-ws/nma-ws-wpa-psk.ui:14
msgid "_Password"
msgstr ""

#: src/nma-ws/nma-eap-peap.c:296 src/nma-ws/nma-eap-ttls.c:350
#: src/nma-ws/nma-ws-802-1x.c:346
msgid "MD5"
msgstr ""

#: src/nma-ws/nma-eap-peap.ui:23
msgid "Automatic"
msgstr ""

#: src/nma-ws/nma-eap-peap.ui:26
msgid "Version 0"
msgstr ""

#: src/nma-ws/nma-eap-peap.ui:29
msgid "Version 1"
msgstr ""

#: src/nma-ws/nma-eap-peap.ui:66 src/nma-ws/nma-eap-tls.ui:38
#: src/nma-ws/nma-eap-ttls.ui:83
msgid "No CA certificate is _required"
msgstr ""

#: src/nma-ws/nma-eap-peap.ui:83
msgid "PEAP _version"
msgstr ""

#: src/nma-ws/nma-eap-peap.ui:162 src/nma-ws/nma-eap-tls.ui:56
#: src/nma-ws/nma-eap-ttls.ui:127
msgid "Suffix of the server certificate name."
msgstr ""

#: src/nma-ws/nma-eap-peap.ui:163 src/nma-ws/nma-eap-tls.ui:57
#: src/nma-ws/nma-eap-ttls.ui:128
msgid "_Domain"
msgstr ""

#: src/nma-ws/nma-eap-simple.c:79
msgid "missing EAP username"
msgstr ""

#: src/nma-ws/nma-eap-simple.c:95
msgid "missing EAP password"
msgstr ""

#: src/nma-ws/nma-eap-simple.c:109
msgid "missing EAP client Private Key passphrase"
msgstr ""

#: src/nma-ws/nma-eap-simple.ui:97
msgid "P_rivate Key Passphrase"
msgstr ""

#: src/nma-ws/nma-eap-simple.ui:122
msgid "Sh_ow passphrase"
msgstr ""

#: src/nma-ws/nma-eap-tls.c:47
msgid "missing EAP-TLS identity"
msgstr ""

#: src/nma-ws/nma-eap-tls.c:234
msgid "no user certificate selected"
msgstr ""

#: src/nma-ws/nma-eap-tls.c:239
msgid "selected user certificate file does not exist"
msgstr ""

#: src/nma-ws/nma-eap-tls.c:259
msgid "no key selected"
msgstr ""

#: src/nma-ws/nma-eap-tls.c:264
msgid "selected key file does not exist"
msgstr ""

#: src/nma-ws/nma-eap-tls.ui:14
msgid "I_dentity"
msgstr ""

#: src/nma-ws/nma-eap-ttls.c:268
msgid "PAP"
msgstr ""

#: src/nma-ws/nma-eap-ttls.c:284
msgid "MSCHAP"
msgstr ""

#: src/nma-ws/nma-eap-ttls.c:317
msgid "MSCHAPv2 (no EAP)"
msgstr ""

#: src/nma-ws/nma-eap-ttls.c:334
msgid "CHAP"
msgstr ""

#: src/nma-ws/nma-eap.c:40
msgid "undefined error in 802.1X security (wpa-eap)"
msgstr ""

#: src/nma-ws/nma-eap.c:348
msgid "no CA certificate selected"
msgstr ""

#: src/nma-ws/nma-eap.c:353
msgid "selected CA certificate file does not exist"
msgstr ""

#: src/nma-ws/nma-ws-802-1x.c:358
msgid "TLS"
msgstr ""

#: src/nma-ws/nma-ws-802-1x.c:382
msgid "PWD"
msgstr ""

#: src/nma-ws/nma-ws-802-1x.c:393
msgid "FAST"
msgstr ""

#: src/nma-ws/nma-ws-802-1x.c:404
msgid "Tunneled TLS"
msgstr ""

#: src/nma-ws/nma-ws-802-1x.c:415
msgid "Protected EAP (PEAP)"
msgstr ""

#: src/nma-ws/nma-ws-802-1x.c:430
msgid "Unknown"
msgstr ""

#: src/nma-ws/nma-ws-802-1x.c:444
msgid "Externally configured"
msgstr ""

#: src/nma-ws/nma-ws-802-1x.ui:25 src/nma-ws/nma-ws-wep-key.ui:95
#, fuzzy
msgid "Au_thentication"
msgstr "Icyerekezo"

#: src/nma-ws/nma-ws-leap.c:71
msgid "missing leap-username"
msgstr ""

#: src/nma-ws/nma-ws-leap.c:87
msgid "missing leap-password"
msgstr ""

#: src/nma-ws/nma-ws-sae.c:73
msgid "missing password"
msgstr ""

# #-#-#-#-#  sw.pot (PACKAGE VERSION)  #-#-#-#-#
# sw/source\ui\index\cnttab.src:TP_TOX_SELECT.FT_SORTALG.text
# #-#-#-#-#  sw.pot (PACKAGE VERSION)  #-#-#-#-#
# sw/source\ui\misc\srtdlg.src:DLG_SORTING.FT_KEYTYP.text
#: src/nma-ws/nma-ws-sae.ui:44 src/nma-ws/nma-ws-wpa-psk.ui:43
#, fuzzy
msgid "_Type"
msgstr "Ubwoko bw'agakandisho"

#: src/nma-ws/nma-ws-wep-key.c:110
msgid "missing wep-key"
msgstr ""

#: src/nma-ws/nma-ws-wep-key.c:117
#, c-format
msgid "invalid wep-key: key with a length of %zu must contain only hex-digits"
msgstr ""

#: src/nma-ws/nma-ws-wep-key.c:125
#, c-format
msgid ""
"invalid wep-key: key with a length of %zu must contain only ascii characters"
msgstr ""

#: src/nma-ws/nma-ws-wep-key.c:131
#, c-format
msgid ""
"invalid wep-key: wrong key length %zu. A key must be either of length 5/13 "
"(ascii) or 10/26 (hex)"
msgstr ""

#: src/nma-ws/nma-ws-wep-key.c:138
msgid "invalid wep-key: passphrase must be non-empty"
msgstr ""

#: src/nma-ws/nma-ws-wep-key.c:140
msgid "invalid wep-key: passphrase must be shorter than 64 characters"
msgstr ""

#: src/nma-ws/nma-ws-wep-key.ui:12
msgid "Open System"
msgstr ""

#: src/nma-ws/nma-ws-wep-key.ui:15
msgid "Shared Key"
msgstr ""

#: src/nma-ws/nma-ws-wep-key.ui:26
msgid "1 (Default)"
msgstr ""

#: src/nma-ws/nma-ws-wep-key.ui:48
msgid "_Key"
msgstr ""

#: src/nma-ws/nma-ws-wep-key.ui:77
msgid "Sho_w key"
msgstr ""

#: src/nma-ws/nma-ws-wep-key.ui:128
msgid "WEP inde_x"
msgstr ""

#: src/nma-ws/nma-ws-wpa-psk.c:80
#, c-format
msgid ""
"invalid wpa-psk: invalid key-length %zu. Must be [8,63] bytes or 64 hex "
"digits"
msgstr ""

#: src/nma-ws/nma-ws-wpa-psk.c:87
msgid "invalid wpa-psk: cannot interpret key with 64 bytes as hex"
msgstr ""

#: src/nma-ws/nma-ws.c:38
msgid "Unknown error validating 802.1X security"
msgstr ""

#. The %s is a mobile provider name, eg "T-Mobile"
#: src/utils/utils.c:161
#, fuzzy, c-format
msgid "%s connection"
msgstr "urusobe Ukwihuza"

#: src/utils/utils.c:535
msgid "PEM certificates (*.pem, *.crt, *.cer)"
msgstr ""

#: src/utils/utils.c:547
msgid "DER, PEM, or PKCS#12 private keys (*.der, *.pem, *.p12, *.key)"
msgstr ""

#: src/wifi.ui:97
msgid "Wi-Fi _security"
msgstr ""

#: src/wifi.ui:129
#, fuzzy
msgid "_Network name"
msgstr "ni OYA"

#: src/wifi.ui:154
#, fuzzy
msgid "C_onnection"
msgstr "Kwihuza"

#: src/wifi.ui:179
msgid "Wi-Fi _adapter"
msgstr ""

#, fuzzy
#~ msgid "Manage your network connections"
#~ msgstr "urusobe Ukwihuza"

#, fuzzy
#~ msgid "Advanced Network Configuration"
#~ msgstr "urusobe Ukwihuza"

#, fuzzy
#~ msgid "NetworkManager"
#~ msgstr "ni OYA"

#, fuzzy
#~ msgid "NetworkManager connection editor"
#~ msgstr "urusobe Ukwihuza"

#, fuzzy
#~ msgid "The NetworkManager Developers"
#~ msgstr "ni OYA"

#, fuzzy
#~ msgid "Connection activation failed"
#~ msgstr "Na: Bishunzwe: Bikora"

#, fuzzy, c-format
#~ msgid ""
#~ "\n"
#~ "The VPN connection “%s” failed."
#~ msgstr "urusobe Ukwihuza"

#, fuzzy
#~ msgid "disconnected"
#~ msgstr "Kwihuza"

#, fuzzy
#~ msgid "Disconnect"
#~ msgstr "Kwihuza"

#, fuzzy
#~ msgid "No network devices available"
#~ msgstr "urusobe Byabonetse"

#, fuzzy
#~ msgid "_Add a VPN connection…"
#~ msgstr "urusobe Ukwihuza"

#, fuzzy
#~ msgid "NetworkManager is not running…"
#~ msgstr "ni OYA"

#, fuzzy
#~ msgid "Edit Connections…"
#~ msgstr "urusobe Ukwihuza"

#, fuzzy
#~ msgid "_About"
#~ msgstr "Bigyanye"

#, fuzzy
#~ msgid "Disconnected"
#~ msgstr "Kwihuza"

#, fuzzy, c-format
#~ msgid "Preparing network connection “%s”…"
#~ msgstr "urusobe Ukwihuza"

#, fuzzy, c-format
#~ msgid "Requesting a network address for “%s”…"
#~ msgstr "Kuri urusobe"

#, fuzzy, c-format
#~ msgid "Network connection “%s” active"
#~ msgstr "urusobe Ukwihuza"

#, fuzzy, c-format
#~ msgid "Starting VPN connection “%s”…"
#~ msgstr "urusobe Ukwihuza"

#, fuzzy, c-format
#~ msgid "Requesting a VPN address for “%s”…"
#~ msgstr "Kuri urusobe"

#, fuzzy
#~ msgid "VPN connection active"
#~ msgstr "urusobe Ukwihuza"

#, fuzzy
#~ msgid "No network connection"
#~ msgstr "urusobe Ukwihuza"

#, fuzzy
#~ msgid "New Mobile Broadband connection…"
#~ msgstr "urusobe Ukwihuza"

#, fuzzy
#~ msgid "Connection Established"
#~ msgstr "Na: Bishunzwe: Bikora"

#, fuzzy, c-format
#~ msgid "Preparing mobile broadband connection “%s”…"
#~ msgstr "urusobe Ukwihuza"

#, fuzzy, c-format
#~ msgid "Configuring mobile broadband connection “%s”…"
#~ msgstr "urusobe Ukwihuza"

#, fuzzy, c-format
#~ msgid "Mobile broadband connection “%s” active"
#~ msgstr "urusobe Ukwihuza Kuri"

#, fuzzy
#~ msgid "Ethernet Network"
#~ msgstr "Kuri"

#, fuzzy, c-format
#~ msgid "Preparing ethernet network connection “%s”…"
#~ msgstr "urusobe Ukwihuza"

#, fuzzy, c-format
#~ msgid "Configuring ethernet network connection “%s”…"
#~ msgstr "urusobe Ukwihuza Kuri"

#, fuzzy, c-format
#~ msgid "User authentication required for ethernet network connection “%s”…"
#~ msgstr "urusobe Ukwihuza"

#, fuzzy, c-format
#~ msgid "Requesting an ethernet network address for “%s”…"
#~ msgstr "Kuri urusobe"

#, fuzzy, c-format
#~ msgid "Ethernet network connection “%s” active"
#~ msgstr "urusobe Ukwihuza Kuri"

#, fuzzy
#~ msgid "_Connect to Hidden Wi-Fi Network…"
#~ msgstr "Kuri a urusobe"

#, fuzzy
#~ msgid "Create _New Wi-Fi Network…"
#~ msgstr "Gishya"

#, fuzzy
#~ msgid "Failed to add new connection"
#~ msgstr "urusobe Ukwihuza"

#, fuzzy
#~ msgid "Wi-Fi Network"
#~ msgid_plural "Wi-Fi Networks"
#~ msgstr[0] "Kuri"
#~ msgstr[1] "Kuri"

#, fuzzy
#~ msgid "More networks"
#~ msgstr "urusobe"

#, fuzzy, c-format
#~ msgid "Preparing Wi-Fi network connection “%s”…"
#~ msgstr "urusobe Ukwihuza"

#, fuzzy, c-format
#~ msgid "Configuring Wi-Fi network connection “%s”…"
#~ msgstr "urusobe Ukwihuza"

#, fuzzy, c-format
#~ msgid "Requesting a Wi-Fi network address for “%s”…"
#~ msgstr "Kuri urusobe"

#, fuzzy, c-format
#~ msgid "Wi-Fi network connection “%s” active: %s (%d%%)"
#~ msgstr "urusobe Ukwihuza Kuri"

#, fuzzy, c-format
#~ msgid "Wi-Fi network connection “%s” active"
#~ msgstr "urusobe Ukwihuza"

#, fuzzy
#~ msgid "Failed to activate connection"
#~ msgstr "urusobe Ukwihuza"

# #-#-#-#-#  sw.pot (PACKAGE VERSION)  #-#-#-#-#
# sw/source\ui\index\cnttab.src:TP_TOX_SELECT.FT_SORTALG.text
# #-#-#-#-#  sw.pot (PACKAGE VERSION)  #-#-#-#-#
# sw/source\ui\misc\srtdlg.src:DLG_SORTING.FT_KEYTYP.text
#, fuzzy
#~ msgid "VPN Type"
#~ msgstr "Ubwoko bw'agakandisho"

#, fuzzy
#~ msgid ""
#~ "Notification area applet for managing your network devices and "
#~ "connections."
#~ msgstr "Ubuso Apuleti... kugirango urusobe Na Ukwihuza"

#, fuzzy
#~ msgid "NetworkManager Website"
#~ msgstr "ni OYA"

#, fuzzy
#~ msgid ""
#~ "The NetworkManager Applet could not find some required resources (the .ui "
#~ "file was not found)."
#~ msgstr "OYA Gushaka Bya ngombwa i IDOSIYE OYA Byabonetse"

#, fuzzy, c-format
#~ msgid "Failed to write connection to VPN UI: %s (%d)"
#~ msgstr "urusobe Ukwihuza"

#, fuzzy
#~ msgid "Bonded _connections"
#~ msgstr "urusobe Ukwihuza"

#, fuzzy
#~ msgid "Bridged _connections"
#~ msgstr "urusobe Ukwihuza"

# basctl/source\basicide\basidesh.src:RID_IMGBTN_REMOVEWATCH.text
#, fuzzy
#~ msgid "s"
#~ msgstr "%s"

#, fuzzy
#~ msgid "_Metered connection"
#~ msgstr "urusobe Ukwihuza"

#, fuzzy
#~ msgid "Connected"
#~ msgstr "Kwihuza"

#, fuzzy
#~ msgid "Authentication"
#~ msgstr "Icyerekezo"

#, fuzzy
#~ msgid "Ethernet port state"
#~ msgstr "Kuri"

#, fuzzy
#~ msgid "_Teamed connections"
#~ msgstr "urusobe Ukwihuza"

#, fuzzy
#~ msgid "Could not create new connection"
#~ msgstr "urusobe Ukwihuza"

#, fuzzy
#~ msgid "_New Connection"
#~ msgstr "urusobe Ukwihuza"

#, fuzzy
#~ msgid "Missing connection name"
#~ msgstr "urusobe Ukwihuza"

#, fuzzy
#~ msgid ""
#~ "The connection editor could not find some required resources (the .ui "
#~ "file was not found)."
#~ msgstr "OYA Gushaka Bya ngombwa i IDOSIYE OYA Byabonetse"

#, fuzzy
#~ msgid "Could not create connection"
#~ msgstr "urusobe Ukwihuza"

#, fuzzy
#~ msgid "Could not edit connection"
#~ msgstr "urusobe Ukwihuza"

#, fuzzy
#~ msgid "Connection _name"
#~ msgstr "urusobe Ukwihuza"

# offmgr/source\offapp\intro\intro.hrc:TEXT_DEFAULTABOUT.text
#, fuzzy
#~ msgid "_Export…"
#~ msgstr "Ibyerekeye"

#, fuzzy
#~ msgid "Connection cannot be deleted"
#~ msgstr "Na: Bishunzwe: Bikora"

#, fuzzy
#~ msgid "Select a connection to edit"
#~ msgstr "urusobe Ukwihuza"

#, fuzzy
#~ msgid "Select a connection to delete"
#~ msgstr "urusobe Ukwihuza"

#, fuzzy
#~ msgid "Edit the selected connection"
#~ msgstr "urusobe Ukwihuza"

#, fuzzy
#~ msgid "Unrecognized connection type"
#~ msgstr "urusobe Ukwihuza"

#, fuzzy, c-format
#~ msgid "Don’t know how to import “%s” connections"
#~ msgstr "urusobe Ukwihuza"

#, fuzzy
#~ msgid "Error importing connection"
#~ msgstr "urusobe Ukwihuza"

#, fuzzy
#~ msgid "Error creating connection"
#~ msgstr "urusobe Ukwihuza"

#, fuzzy
#~ msgid "Connection type not specified."
#~ msgstr "Na: Bishunzwe: Bikora"

#, fuzzy, c-format
#~ msgid "Don’t know how to create “%s” connections"
#~ msgstr "urusobe Ukwihuza"

#, fuzzy
#~ msgid "Error editing connection"
#~ msgstr "urusobe Ukwihuza"

#, fuzzy
#~ msgid "Network Connections"
#~ msgstr "urusobe Ukwihuza"

#, fuzzy
#~ msgid "Add a new connection"
#~ msgstr "urusobe Ukwihuza"

#, fuzzy, c-format
#~ msgid "Bluetooth connection %d"
#~ msgstr "urusobe Ukwihuza"

#, fuzzy
#~ msgid "_Personal Area Network"
#~ msgstr "Kuri"

#, fuzzy
#~ msgid "_Dial-Up Network"
#~ msgstr "Kuri"

#, fuzzy, c-format
#~ msgid "Bond connection %d"
#~ msgstr "urusobe Ukwihuza"

#, fuzzy, c-format
#~ msgid "Bridge connection %d"
#~ msgstr "urusobe Ukwihuza"

#, fuzzy, c-format
#~ msgid "Ethernet connection %d"
#~ msgstr "urusobe Ukwihuza"

#, fuzzy, c-format
#~ msgid "IP tunnel connection %d"
#~ msgstr "urusobe Ukwihuza"

#, fuzzy, c-format
#~ msgid "MACSEC connection %d"
#~ msgstr "urusobe Ukwihuza"

#, fuzzy, c-format
#~ msgid "Team connection %d"
#~ msgstr "urusobe Ukwihuza"

#, fuzzy
#~ msgid "New connection…"
#~ msgstr "urusobe Ukwihuza"

#, fuzzy, c-format
#~ msgid "VLAN connection %d"
#~ msgstr "urusobe Ukwihuza"

#, fuzzy, c-format
#~ msgid "VPN connection %d"
#~ msgstr "urusobe Ukwihuza"

#, fuzzy, c-format
#~ msgid "Wi-Fi connection %d"
#~ msgstr "urusobe Ukwihuza"

#, fuzzy
#~ msgid "Cannot export VPN connection"
#~ msgstr "urusobe Ukwihuza"

#, fuzzy
#~ msgid "Export VPN connection…"
#~ msgstr "urusobe Ukwihuza"

#, fuzzy
#~ msgid "C_onnection:"
#~ msgstr "Kwihuza"

#, fuzzy
#~ msgid "Home network"
#~ msgstr "Kugena urusobe"

#, fuzzy
#~ msgid "Roaming network"
#~ msgstr "Kuri"

#, fuzzy, c-format
#~ msgid "Mobile broadband connection “%s” active: (%d%%%s%s)"
#~ msgstr "urusobe Ukwihuza Kuri"

#, fuzzy
#~ msgid "Connection _priority for auto-activation:"
#~ msgstr "Na: Bishunzwe: Bikora"

#, fuzzy
#~ msgid "Cannot import VPN connection"
#~ msgstr "urusobe Ukwihuza"

#, fuzzy
#~ msgid "NetworkManager for GNOME"
#~ msgstr "ni OYA"

#, fuzzy
#~ msgid "_Disconnect VPN"
#~ msgstr "Kwihuza"

#, fuzzy
#~ msgid "_Connect to Hidden Wi-Fi Network..."
#~ msgstr "Kuri a urusobe"

#, fuzzy
#~ msgid "Create _New Wi-Fi Network..."
#~ msgstr "Gishya"

#~ msgid " "
#~ msgstr " "

#, fuzzy
#~ msgid "FirewallD is not running."
#~ msgstr "ni OYA"

#, fuzzy
#~ msgid "<span weight=\"bold\">Wireless Networks:</span>"
#~ msgstr "<Uburemere UTSINDAGIYE"

#~ msgid "*"
#~ msgstr "*"

#, fuzzy
#~ msgid ""
#~ "<span weight=\"bold\" size=\"larger\">Passphrase Required by Wireless "
#~ "Network</span>\n"
#~ "\n"
#~ "A passphrase or encryption key is required to access the wireless network "
#~ "'%s'."
#~ msgstr ""
#~ "<Uburemere UTSINDAGIYE Ingano Kinini ku A Cyangwa Bishunzwe: Urufunguzo "
#~ "ni Bya ngombwa Kuri i urusobe"

#, fuzzy
#~ msgid "You must log in to access the private network %s"
#~ msgstr "LOG in Kuri i By'umwihariko urusobe"

#, fuzzy
#~ msgid "Copyright (C) 2004-2005 Red Hat, Inc."
#~ msgstr "C"

#, fuzzy
#~ msgid "The network device \"%s (%s)\" does not support wireless scanning."
#~ msgstr "urusobe APAREYE OYA Gushigikira"

#, fuzzy
#~ msgid "The network device \"%s (%s)\" does not support link detection."
#~ msgstr "urusobe APAREYE OYA Gushigikira Ihuza"

#, fuzzy
#~ msgid "Connected to an Ad-Hoc wireless network"
#~ msgstr "Kuri urusobe"

#, fuzzy
#~ msgid "Scanning for wireless networks..."
#~ msgstr "kugirango"

#, fuzzy
#~ msgid "Start All Wireless Devices"
#~ msgstr "Gutangira"

# scp/source\office\profile.lng:STR_VALUE_TEMPLATE_SERVICES_HELP.text
#~ msgid "Help"
#~ msgstr "Ifashayobora"

#, fuzzy
#~ msgid "Create new wireless network"
#~ msgstr "Gishya urusobe"

#, fuzzy
#~ msgid ""
#~ "Enter the ESSID of the wireless network to which you wish to connect."
#~ msgstr "i Bya i urusobe Kuri Kuri Kwihuza"

#, fuzzy
#~ msgid ""
#~ "128-bit passphrase (WEP)\n"
#~ "Ascii key (WEP)\n"
#~ "Hex key (WEP)"
#~ msgstr "Urufunguzo Urufunguzo"

# #-#-#-#-#  sw.pot (PACKAGE VERSION)  #-#-#-#-#
# sw/source\ui\index\cnttab.src:TP_TOX_SELECT.FT_SORTALG.text
# #-#-#-#-#  sw.pot (PACKAGE VERSION)  #-#-#-#-#
# sw/source\ui\misc\srtdlg.src:DLG_SORTING.FT_KEYTYP.text
#, fuzzy
#~ msgid "Key type:"
#~ msgstr "Ubwoko bw'agakandisho"

#, fuzzy
#~ msgid " (invalid Unicode)"
#~ msgstr "(Sibyo"

#, fuzzy
#~ msgid "The orientation of the tray."
#~ msgstr "Icyerekezo Bya i"

#, fuzzy
#~ msgid ""
#~ "<span weight=\"bold\" size=\"larger\">Reduced Network Functionality</"
#~ "span>\n"
#~ "\n"
#~ "%s  It will not be completely functional."
#~ msgstr "<Uburemere UTSINDAGIYE Ingano Kinini OYA"

#, fuzzy
#~ msgid ""
#~ "<span weight=\"bold\" size=\"larger\">Wireless Network Login "
#~ "Confirmation</span>\n"
#~ "\n"
#~ "You have chosen log in to the wireless network '%s'.  If you are sure "
#~ "this wireless network is secure, click the checkbox below and "
#~ "NetworkManager will no longer pester you with stupid questions when you "
#~ "connect to it."
#~ msgstr ""
#~ "<Uburemere UTSINDAGIYE Ingano Kinini LOG in Kuri i urusobe iyi urusobe ni "
#~ "Kanda i munsi Na Oya Na: Ryari: Kwihuza Kuri"

#, fuzzy
#~ msgid "Always Trust this Wireless Network"
#~ msgstr "iyi"

#, fuzzy
#~ msgid ""
#~ "unable to create netlink socket for monitoring wired ethernet devices - %s"
#~ msgstr "Kuri Kurema kugirango"

#, fuzzy
#~ msgid ""
#~ "unable to bind to netlink socket for monitoring wired ethernet devices - "
#~ "%s"
#~ msgstr "Kuri Kuri kugirango"

#, fuzzy
#~ msgid "received data from wrong type of sender"
#~ msgstr "BYAKIRIWE Ibyatanzwe Bivuye Ubwoko Bya Uwohereza"

#, fuzzy
#~ msgid "received data from unexpected sender"
#~ msgstr "BYAKIRIWE Ibyatanzwe Bivuye Uwohereza"

#, fuzzy
#~ msgid "too much data was sent over socket and some of it was lost"
#~ msgstr "Ibyatanzwe Yoherejwe: KURI Na Bya"
